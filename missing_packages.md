TODO: package imports (disabled arch repos) to build

### Proposal:

* all packages for official profiles must exist in artix repos

* community profiles should have as many packages possible in artix repos, but eg FF, TB, libreoffice are excluded for now

* please add here package lists for other profiles

### Missing

* lxqt

~~~
error: target not found: cmst
error: target not found: qpdfview
~~~

* community-qt

~~~
error: target not found: xorg-xwininfo
error: target not found: cvs
error: target not found: hddtemp
error: target not found: wine-mono
error: target not found: wine-gecko
error: target not found: winetricks
error: target not found: qpdfview
error: target not found: trojita
error: target not found: veracrypt
error: target not found: firefox
error: target not found: firefox-dark-reader
error: target not found: firefox-adblock-plus
error: target not found: firefox-noscript
error: target not found: firefox-ublock-origin
error: target not found: libreoffice-fresh
error: target not found: youtube-dl
error: target not found: fortune-mod
error: target not found: redshift
error: target not found: torsocks
error: target not found: lib32-mesa-vdpau
error: target not found: libva-vdpau-driver
error: target not found: libva-intel-driver
error: target not found: mesa-demos
error: target not found: chafa
error: target not found: tigervnc
error: target not found: ltrace
error: target not found: modem-manager-gui
error: target not found: geany-plugins
error: target not found: syncthing-gtk
error: target not found: handbrake
error: target not found: audacity
error: target not found: gimp
error: target not found: hexchat
error: target not found: gufw
error: target not found: gtk-engines
error: target not found: picom
error: target not found: digikam
error: target not found: smb4k
error: target not found: clementine
error: target not found: kvantum-qt5
error: target not found: qt5ct
error: target not found: mc
~~~

* xfce

~~~
error: target not found: pavucontrol
error: target not found: qt5ct
error: target not found: lightdm-gtk-greeter
error: target not found: midori
error: target not found: leafpad
~~~

* i3

~~~
error: target not found: xorg-xkill
error: target not found: lightdm-gtk-greeter
error: target not found: geany
error: target not found: arandr
error: target not found: sysstat
error: target not found: otf-font-awesome
error: target not found: nitrogen
error: target not found: menumaker
error: target not found: rofi
error: target not found: xcursor-neutral
error: target not found: archlinux-xdg-menu
error: target not found: paprefs
error: target not found: pavucontrol
error: target not found: picom
error: target not found: leafpad
error: target not found: pcmanfm
error: target not found: feh
error: target not found: lxappearance-gtk3
error: target not found: lxterminal
error: target not found: midori
error: target not found: viewnior
error: target not found: qt5ct
error: target not found: kvantum-qt5
~~~

* gnome

~~~
error: target not found: arc-gtk-theme
error: target not found: xcursor-themes
error: target not found: gnome-screensaver
error: target not found: pavucontrol
error: target not found: kvantum-qt5
error: target not found: qt5ct
error: target not found: mate-icon-theme-faenza
error: target not found: midori
error: target not found: leafpad
~~~

